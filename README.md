# O2O Connect  #

For WPG 大大家專案，提供大聯大(WPG)的合作夥伴 B2B 接口。

---

### 連線須知 ###

* 請先確認貴公司是 WPG 的 第三方物流(3PL) 或是 Fordwarder(TMS) 或是客戶(Buyer)
* REST/SOAP 用戶請先與 WPG or 億科聯繫取得帳號/密碼 
    * 資料格式為 XML (SOAP) or JSON(REST)
* 如果想使用 EDI 請與 WPG or 億科聯繫，並提供 EDI 連線資訊
其他詳細資訊請參考 [WIKI](https://bitbucket.org/echannelopen/o2o_public/wiki/Home)





